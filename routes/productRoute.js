const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// Route for creating a course
router.post("/", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

//Route for retrieving all the courses
router.get("/all",(req,res)=>{
	productController.getAllProducts().then(resultFromController=>res.send(
		resultFromController));
});
router.get("/active-products",(req,res)=>{
	productController.activeProducts().then(resultFromController=>res.send(
		resultFromController));
});
router.get("/orders",auth.verify,(req,res)=>{
	const userData =auth.decode(req.headers.authorization);
	//console.log(userData);	
	productController.getOrders({userId:userData.id}).then(resultFromController=> res.send(
		resultFromController));
})


router.put("/:productId", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  const isAdmin = userData.isAdmin;
  productController.updateProduct(isAdmin, req.params, req.body)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(403).send({ error: error }));
});

router.put("/archive/:productId",auth.verify,(req,res)=>{
	const userData = auth.decode(req.headers.authorization);
  const isAdmin = userData.isAdmin;
	productController.archiveProduct(isAdmin,req.params,req.body).then(
		resultFromController=>res.send(resultFromController))
	.catch(error=>res.status(403).send({error:error}));
})
router.get("/:productId",(req,res)=>{
	console.log(req.params.productId);
	productController.getProduct(req.params).then(resultFromController=> res.send(resultFromController));

});

module.exports = router;