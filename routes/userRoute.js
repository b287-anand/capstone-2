const express=require("express");
const router =express.Router();
const auth=require("../auth");
const userController=require("../controllers/userController");


router.post("/checkEmail",(req,res)=>
	{
	userController.checkEmailExists(req.body).then(resultFromController=> res.send(
		resultFromController));
	});
router.post("/register",(req,res)=>
{
	userController.registerUser(req.body).then(resultFromController=> res.send(
		resultFromController));
});
router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(resultFromController=> res.send(
		resultFromController));
})
router.get("/myorders/",auth.verify,(req,res)=>{
	const Data =auth.decode(req.headers.authorization);
	
	userController.getMyOrders({userId:Data.id}).then(resultFromController1=> res.send(
		resultFromController1));
})
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  const isAdmin = userData.isAdmin; // Get the isAdmin status from the decoded token

  // Pass isAdmin flag to the setAsAdmin function
  userController.setAsAdmin(req.params, req.body, isAdmin).then((resultFromController) => res.send(resultFromController));
});
router.get("/:userId",auth.verify,(req,res)=>{
	const userData =auth.decode(req.headers.authorization);
	//console.log(userData);	
	userController.getDetails({userId:userData.id}).then(resultFromController=> res.send(
		resultFromController));
})



router.post("/checkout",auth.verify,(req,res)=>{
	let data={
		userId:auth.decode(req.headers.authorization).id,
		productId:req.body.productId,
		productName:req.body.productName,
		quantity:req.body.quantity



	};
	console.log(data);
	userController.checkout(data).then(resultFromController=>res.send(
		resultFromController));
});


module.exports=router;
