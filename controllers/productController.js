const Product = require("../models/Product");

module.exports.addProduct = (data) => {

	console.log(data);

	if(data.isAdmin){

		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		});

		
		return newProduct.save().then((product, error) => {

		
			if(error){

				return false;

			} else {

				return ("New Bike Added Successfully");
			}
		})


	}

	let message = Promise.resolve("User must be an Admin to access this.");
	return message.then((value) => {
		return value
	})
	
}
;
module.exports.getAllProducts =() =>{
	return Product.find({}).then(result=>{
		return result;
	});
};
module.exports.activeProducts=()=>{
	return Product.find({isActive:true}).then(result=>{
		return result;
	});
};
module.exports.getProduct=(reqParams)=>{
	return Product.findById(reqParams.productId).then(result=>{
		return result;
	});
};
module.exports.getOrders=(data)=>{
	if(data.isAdmin){
		return Products.find({}).select("userOrders").then(result=>{
			return result;
		})
	}
	else{
		return("User must be an admin to access this")
	}
}
module.exports.updateProduct = (isAdmin, reqParams, reqBody) => {
  if (isAdmin) {
    let updatedProduct = {
      name: reqBody.name,
      description: reqBody.description,
      price: reqBody.price
    };

    return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
      .then((product) => {
        return "The product info has been updated";
      })
      .catch((error) => {
        return false;
      });
  } else {
    return Promise.reject("User must be an admin to access this");
  }
};
module.exports.archiveProduct=(isAdmin,reqParams,reqBody)=>{
	if(isAdmin){
	let archivedProduct={
		
		isActive:reqBody.isActive

	};
	return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((
		product,error)=>{
		if(error){
			return false
		}else{
			return (`The Status has been updated`)
		}
	})}
	else{
		return Promise.reject("User must be an admin to access this");

	}

};