const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product= require("../models/Product");


module.exports.checkEmailExists = (data) => {

	console.log(data.email); 


	return User.findOne({email:data.email}).then(result => {

		console.log(result);
		if(result!==null){
		
		return true;}
		else{
			return false;
		}

	})
}



module.exports.registerUser = (reqBody) => {

	console.log(reqBody);

	let newUser = new User({
		
		email: reqBody.email,
	
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if(error){
			return false
		} else {
			return true ;
		}
	})
}

module.exports.loginUser = (reqBody) => {

	// .findOne() will look for the first document that matches
	return User.findOne({ email: reqBody.email }).then(result => {

		console.log(result);

		// Email doesn't exist
		if(result == null){

			return false;

		} else {

			// We now know that the email is correct, is password correct also?
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			console.log(isPasswordCorrect);
			console.log(result);

			// Correct password
			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result) }

			// Password incorrect	
			} else {

				return false
				
			};

		};
	});
};

module.exports.getDetails = (data) => {

	console.log(data.userId); 

	return User.findById(data.userId).then(result => {
		result.password = "";
		console.log(result);
		
		
		return result;

	})
}
module.exports.getMyOrders = (data) => {

	console.log(data.userId); 

	return User.findById(data.userId).select("products").then(result => {
		console.log(result);
		
		
		
		return result;

	})
}


module.exports.setAsAdmin = (reqParams, reqBody, isAdmin) => {
  if (!isAdmin) {let message = Promise.resolve("User must be a Admin to access this.");
  return message.then((value) => {
		return value
	})}
  else{


   

  let changeAdmin = {
    isAdmin: reqBody.isAdmin
  };

  return User.findByIdAndUpdate(reqParams.userId, changeAdmin).then((user, error) => {
    if (error) {
      return false;
    } else {
      return "The Status has been updated";
    }
  });}
};



module.exports.checkout = async (data) => {
	if( data.isAdmin){
		let message = Promise.resolve("User must be a non-Admin to access this.");
	return message.then((value) => {
		return value
	})}
	else{
  let isUserUpdated = await User.findById(data.userId).then(user => {
    user.products.push({
      productId: data.productId,
      productName: data.productName,
      quantity: data.quantity
    });
    return user.save().then((user, error) => {
      if (error) {
        return false;
      } else {
        return "Successful";
      }
    });
  });

  let isProductUpdated = await Product.findById(data.productId).then(product => {
    product.userOrders.push({ userId: data.userId });
    return product.save().then((product, error) => {
      if (error) {
        return false;
      } else {
        return "Successful";
      }
    });
  });

  console.log(isUserUpdated);
  console.log(isProductUpdated);

  if (isUserUpdated === "Successful" && isProductUpdated === "Successful") {
    return "Successfully placed your order";
  } else {
    return false;
  }}
  
	
};
