const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: [true, "This data is required"],
  },
  password: {
    type: String,
    required: [true, "This data is required"],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  products: [
    {
      productId: {
        type: String,
        required: [true, "This data is required."],
      },
      productName: {
        type: String,
        required: [true, "This data is required."],
      },
      quantity: {
        type: String,
        required: [true, "This data is required."],
      },
    }
  ],
  totalAmount: {
    type: Number,
   
  },
  purchasedOn: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("User", userSchema);
